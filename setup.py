from   setuptools import setup, find_packages
from   dotenv     import load_dotenv
import os

# load environment variables from .env
load_dotenv()

# helper function to load environement variable as bool
def parse_bool(var: str, 
               default: str = "y") -> bool:
    return (
        os.getenv(var, default).lower() in ("1", "true", "yes", "y")
    )

# default settings dictionary
settings = {
    "name"                : os.getenv("PACKAGE_NAME"),
    "version"             : os.getenv("VERSION"),
    "author"              : os.getenv("AUTHOR"),
    "author_email"        : os.getenv("EMAIL"),
    "description"         : os.getenv("DESCRIPTION"),
    "packages"            : find_packages(),
    "include_package_data": parse_bool("INCLUDE_DATA"),
}

# get absolute path to README.md
pwd    = os.path.abspath(os.path.dirname(__file__))
readme = os.path.join(pwd, 'README.md')

# check if README.md exists, if it does add as long
# description to seetings
if parse_bool("INCLUDE_README") and os.path.exists(readme):
    with open(readme, encoding='utf-8') as f:
        settings["long_description"] = f.read()
        settings["long_description_content_type"] = "text/markdown"

# truncate long_description to README.md for readability
print_settings = {
    k:v if k != 'long_description' else 'README.md'
    for k,v in settings.items()
}

# print configured values to screen
print(f"""Configured Values:\n{print_settings}""")

# run setup 
setup(**settings)

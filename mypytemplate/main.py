__all__ = ["templateFunction"]

def templateFunction():
    """Template Function 

    An function example for testing autdocumentation
    and distribution purposes.
    """

    return "Hello, World!"
# Pure Python Template

This repo contains a base template for a pure python project.  

The Makefile located in the root of the project contains the instructions to test, produce documentation and produce distribution files for a given project.

## Environment File
The variables in the `.env` file in the root of the project are **required** to be populated for the project.

Environment variables:
- PROJECT
    - Name of the project
- VERSION
    - Project version
- PACKAGE_NAME
    - Name of package (should match package name e.g. 'mypytemplate')
- AUTHOR
    - Author Name
- EMAIL
    - Author / support email
- DESCRIPTION
    - Project description 
    - README.md will be used by default as the long description
- INCLUDE_DATA
    - Optional, default is False
    - If True MANIFEST.in will be used to load files into distribution files

## Makefile Instructions
Navigate to the root of the project and type one of the following commands.

**NOTE**:  
This assumes make is installed on your os


### Environment
To install environment requirements:
```
make install-reqs
```

To save environment requirements:
```
make update-reqs
```

### Testing
To run testing for the project:
```
make test
```
or, verbosely:
```
make test-verbose
```

### Distribution
To produce an egg file:
```
make egg
```

To produce a wheel file:
```
make wheel
```

### Documentation
To produce sphinx read the doc themed documentation:
```
make rtd
```

To run a server to view documents in browser:
```
make doc
```

To regenerate documentation and run server:
```
make rtd-show
```
#!make
include .env

test: ## run all unittests
	@echo Testing ${PROJECT}: ${VERSION} \
	&& python -m unittest discover tests/ "*Test.py"

test-verbose: ## run all unittests
	@echo Testing ${PROJECT}: ${VERSION} in verbose mode \
	&& python -m unittest discover tests/ "*Test.py" -v

egg: ## create egg file
	@python setup.py bdist_egg \
	&& rm -rf build/ ${PACKAGE_NAME}.egg-info

wheel: ## create wheel file
	@python setup.py bdist_wheel \
	&& rm -rf build/ ${PACKAGE_NAME}.egg-info

install-reqs: ## install pip requirements 
	pip install -r requirements.txt

update-reqs: ## update requirements.txt
	@pip freeze > requirements.txt

rtd: ## generate read the docs theme documentation
	@sphinx-apidoc     \
	-feF              \
	--ext-autodoc     \
	--ext-intersphinx \
	--ext-todo        \
	--ext-mathjax     \
	-o ./_docs/   \
	./${PACKAGE_NAME} \
	&& sed -i "s/html_theme.*/html_theme = 'sphinx_rtd_theme'/g" ./_docs/conf.py \
	&& sed -i "s/# import/import/g" ./_docs/conf.py \
	&& sed -i "s/# sys.*/sys.path.insert(0, '..')/g" ./_docs/conf.py \
	&& cd ./_docs/ \
	&& make html \
	&& cd - \
	&& rm -rf ./docs/ \
	&& mv ./_docs/_build/html/ ./docs/ \
	&& rm -rf ./_docs/

doc: ## run documentation server
	@cd docs && python -m http.server

rtd-show: rtd doc ## regenerate documentation and run simple doc server
	

#--------------------------------------------------
# print help
#--------------------------------------------------

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
	| sort \
	| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
